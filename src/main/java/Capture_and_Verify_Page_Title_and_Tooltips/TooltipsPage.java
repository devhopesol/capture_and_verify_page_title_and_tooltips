package Capture_and_Verify_Page_Title_and_Tooltips;

	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;

	public class TooltipsPage {
	    private WebDriver driver;

	    public TooltipsPage(WebDriver driver) {
	        this.driver = driver;
	        driver.get("https://practice.expandtesting.com/tooltips");
	    }

	    public String getPageTitle() {
	        return driver.getTitle();
	    }

	    public String getTooltipText_Tooltip_on_top(String tooltipId) {
	        WebElement tooltipElement = driver.findElement(By.id("btn1"));
	        return tooltipElement.getAttribute("title");
	    }
	}
	
	

