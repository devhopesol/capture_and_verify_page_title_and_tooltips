package Capture_and_Verify_Page_Title_and_Tooltips;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.interactions.Actions;
public class TooltipsPageTest {
    private WebDriver driver;
    private TooltipsPage tooltipsPage;

    @BeforeClass
    public void setUp() {
    	WebDriverManager.firefoxdriver().setup();
        driver =new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        tooltipsPage = new TooltipsPage(driver);
    }

    @Test(priority=1)
    public void testPageTitle() {
        String expectedTitle = "Test Automation Practice: Working with Tooltips";
        Assert.assertEquals(tooltipsPage.getPageTitle(), expectedTitle);
    }

     @Test(priority=2)
    public void testTooltipText_Tooltip_on_top() {
    	Actions actions = new Actions(driver);
        // Find the button element
        WebElement button = driver.findElement(By.id("btn1")); 

        // Perform mouse hover action
        actions.moveToElement(button).perform();

        // Find the tooltip element 
        WebElement tooltip = driver.findElement(By.cssSelector("#btn1")); 

        // Get the tooltip text
        String actualTooltip = tooltip.getText();

        // Compare the actual tooltip with the expected tooltip
        String expectedTooltip = "Tooltip on top";
        if (actualTooltip.equals(expectedTooltip)) {
            System.out.println("Test Case Passed");
        } else {
            System.out.println("Test Case Failed");
        }
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}